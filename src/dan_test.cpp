#include <iostream>

#include "RD53BSimContainer.hh"
#include <inttypes.h>

using namespace std;

int main(int argc, char ** argv) {
    // Create one RD53BSimInterface instance
    RD53BSimInterface simulator("test.yaml");

    std::vector<int> trigger_times = {10,40,70,73};
    std::vector<int> trigger_tags  = {1,2,3,4};
    std::vector<int> trigger_masks = {0x1,0x2,0x4,0x3};
    
    for(size_t iTrig = 0; iTrig < trigger_times.size();iTrig++){
      simulator.Trigger(trigger_times[iTrig],
			trigger_tags[iTrig],
			trigger_masks[iTrig]); //timestamp, triggerTag, triggerMask
      cout << "Event triggered at timestamp: "
	   << std::to_string(trigger_times[iTrig])
	   << endl;
    }
    
    printf("  time  ||      link 1      ||      link 2      ||      link 3      ||      link 4      || trig ||\n");
    for(size_t current_time = 0; current_time < 100; current_time++){
      
      RD53BSimData simdata = simulator.GetChipData(current_time, 0, 0);
      printf("%08u  ",current_time);
      for(size_t elink = 0; elink < 4; elink++) {
	if(simdata.GetValid(elink)){
	  printf("0x%016" PRIX64 "  ",simdata.GetData(elink));
	}else{
	  printf("    ----------      ");
	}
      }
      if(!trigger_times.empty() &&
	 (current_time == trigger_times.front())){
	printf("  *   ");
	trigger_times.erase(trigger_times.begin());
      }
      printf("\n");
    }

    return 0;
}
