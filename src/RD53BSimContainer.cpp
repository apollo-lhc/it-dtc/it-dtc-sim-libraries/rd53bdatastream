#include <RD53BSimContainer.hh>

RD53BSimInterface::RD53BSimInterface(std::string configFile) {
  YAML::Node config = YAML::LoadFile(configFile);
  dataOrg = new RD53BDataOrganizer(config["inputFileName"].as<std::string>().c_str());
  YAML::Node moduleConfigs = config["moduleConfigs"];
  for (size_t imodule=0; imodule<moduleConfigs.size(); imodule++) {
    size_t cur_module_index = moduleConfigs[imodule]["index"].as<size_t>();
    //uint8_t nchips = moduleConfigs[imodule]["nchips"].as<uint8_t>();
    //uint8_t nlinks = moduleConfigs[imodule]["nlinks"].as<uint8_t>();
    uint8_t nchips = moduleConfigs[imodule]["nchips"].as<unsigned int>();
    uint8_t nlinks = moduleConfigs[imodule]["nlinks"].as<unsigned int>();
    RD53BDatastream::module_config cur_module_config = RD53BDatastream::get_config_from_nchips_nlinks(nchips, nlinks);
    dataOrg->add_stream(cur_module_config, cur_module_index);
    vec_moduleID.push_back(cur_module_index);
    for (size_t chipID=0; chipID<nchips; chipID++) {
      map_IDs_to_queue[std::make_pair(cur_module_index, chipID)] = std::queue<std::pair<uint64_t, RD53BSimData> >();
    }
  }
}

void RD53BSimInterface::Trigger(uint64_t currentTime,uint8_t triggerTag, uint8_t triggerMask){
  this->dataOrg->Trigger(currentTime, triggerTag, triggerMask);
  for (size_t istream=0; istream < this->dataOrg->get_nstreams(); istream++){
    vector<RD53BDataContainer> streamdata = this->dataOrg->GetData(istream);
    size_t moduleID = this->vec_moduleID[istream];
    size_t chipIDBase = 0;
    //note that there is either multiple chips or multiple elinks, never both. So treat 2 cases differently
    if (streamdata.size()==1) {//which means single e-link, so one or more chips
      for (int iWord=0; iWord++; iWord<streamdata[0].data.size()) {
        uint64_t chipID = chipIDBase + streamdata[0].word_chip_mapping[iWord];
        RD53BSimData simDataForChip = {};
        simDataForChip.valid[0] = true;
        simDataForChip.data[0] = streamdata[0].data[iWord];
        map_IDs_to_queue[std::make_pair(moduleID, chipID)].emplace(currentTime, simDataForChip);
      }
    }
    else {//which means multiple e-links, with only one chip
      uint64_t chipID = chipIDBase;
      for (int iWord=0; iWord<streamdata[0].data.size(); iWord++) {
        RD53BSimData simDataForChip = {};
        for (int iElink=0; iElink<streamdata.size(); iElink++) {
          if (iWord < streamdata[iElink].data.size()) {
            simDataForChip.valid[iElink] = true;
            simDataForChip.data[iElink] = streamdata[iElink].data[iWord];
          }
        }
        map_IDs_to_queue[std::make_pair(moduleID, chipID)].emplace(currentTime, simDataForChip);
      }
    }
  }
  return;
}

RD53BSimData RD53BSimInterface::GetChipData(uint64_t currentTime, size_t moduleID, size_t chipID){
  if (map_IDs_to_queue[std::make_pair(moduleID, chipID)].size()==0) {
    return RD53BSimData();
  }
  std::pair<uint64_t, RD53BSimData> time_simData = map_IDs_to_queue[std::make_pair(moduleID, chipID)].front();
  if (currentTime <= time_simData.first) {
    return RD53BSimData();
  }
  else {
    map_IDs_to_queue[std::make_pair(moduleID, chipID)].pop();
    return time_simData.second;
  }
}

size_t RD53BSimInterface::GetChipCount() {
  return map_IDs_to_queue.size();
}
