#include <RD53BDatastream.hh>


uint64_t RD53BDataContainer::GetData(size_t iSample){
  return data[iSample];
}

size_t  RD53BDataContainer::GetDataSize() const {
  return data.size();
}

RD53BDatastream::module_config RD53BDatastream::get_config_from_nchips_nlinks(uint8_t nchips, uint8_t nlinks){
  if (nchips == 2) {
    if (nlinks == 2) return TWO_CHIP_TWO_LINK;
    if (nlinks == 3) return TWO_CHIP_THREE_LINK;
    if (nlinks == 6) return TWO_CHIP_SIX_LINK;
    else {
      throw std::invalid_argument("Illegal combination of nchips("+std::to_string(nchips)+") and nlinks("+std::to_string(nlinks)+").");
    }
  }
  else if (nchips == 4) {
    if (nlinks == 1) return FOUR_CHIP_ONE_LINK;
    if (nlinks == 2) return FOUR_CHIP_TWO_LINK;
    if (nlinks == 3) return FOUR_CHIP_THREE_LINK;
    if (nlinks == 4) return FOUR_CHIP_FOUR_LINK;
    else {
      throw std::invalid_argument("Illegal combination of nchips("+std::to_string(nchips)+") and nlinks("+std::to_string(nlinks)+").");
    }
  }
  else {
    throw std::invalid_argument("Illegal number of chips("+std::to_string(nchips)+") in module.");
  }
}

void  RD53BDataContainer::Print(std::ostream & fout) const {
  fout << "========== RD53BDataContainer Print Start ==============" <<std::endl;
  fout << "n_chips = "       << int(nchips)       << std::endl;
  fout << "trigger_tag = "  << std::setfill('0') << std::setw(2)  << std::hex << int(trigger_tag)  << std::endl;
  fout << "trigger_mask = " << std::setfill('0') << std::setw(2)  << std::hex << int(trigger_mask) << std::endl;
  fout << "error_mask = "   << std::setfill('0') << std::setw(16) << std::hex << error_mask << std::endl; //NOLINT: ignore warning for 16 as magic number
  fout << "timestamp = "    << int(timestamp)    << std::endl;
  fout << "len(word_chip_mapping) = " << to_string(word_chip_mapping.size()) << std::endl;
  fout << "len(last_valid_bit) = " << to_string(last_valid_bit.size()) << std::endl;
  fout << "len(data) = " << to_string(data.size()) << std::endl;
  fout << "========== RD53BDataContainer Print End ==============" <<std::endl;
}
void  RD53BDataContainer::PrintData(std::ostream & fout){
  for (auto chipid : word_chip_mapping) {
    fout<<"  "<<int(chipid);
  }
  fout<<std::endl;
  for (auto ilast_valid_bit : last_valid_bit) {
    fout<<"  "<<int(ilast_valid_bit);
  }
  fout<<std::endl;
  for (auto idata : data) {
    fout<<"  " << std::setfill('0') << std::setw(16) << std::hex << idata << std::endl; //NOLINT: ignore warning for 16 as magic number
  }
  fout<<std::endl;
  fout << "========== RD53BDataContainer Print End ==============" <<std::endl;
}




uint64_t append_bool_to_uint64(uint64_t acc, bool new_bool) {
  return ( acc << 1 ) | uint64_t(new_bool);
}

RD53BDatastream::RD53BDatastream(module_config mc)
{
  if (mc == TWO_CHIP_TWO_LINK) {
    n_chips = 2; n_elinks = 2;
    chip_to_link_mapping[0] = {0};
    chip_to_link_mapping[1] = {1};
  }
  else if (mc == TWO_CHIP_THREE_LINK) {
    n_chips = 2; n_elinks = 3;
    chip_to_link_mapping[0] = {0};
    chip_to_link_mapping[1] = {1, 2};
  }
  else if (mc == TWO_CHIP_SIX_LINK) {
    n_chips = 2; n_elinks = 6; // NOLINT
    chip_to_link_mapping[0] = {0, 1, 2};
    chip_to_link_mapping[1] = {3, 4, 5};
  }
  else if (mc == FOUR_CHIP_ONE_LINK) {
    n_chips = 4; n_elinks = 1;
    chip_to_link_mapping[0] = {0};
    chip_to_link_mapping[1] = {0};
    chip_to_link_mapping[2] = {0};
    chip_to_link_mapping[3] = {0};
  }
  else if (mc == FOUR_CHIP_TWO_LINK) {
    n_chips = 4; n_elinks = 2;
    chip_to_link_mapping[0] = {0};
    chip_to_link_mapping[1] = {1};
    chip_to_link_mapping[2] = {0};
    chip_to_link_mapping[3] = {1};
  }
  else if (mc == FOUR_CHIP_THREE_LINK) {
    n_chips = 4; n_elinks = 3;
    chip_to_link_mapping[0] = {0};
    chip_to_link_mapping[1] = {0};
    chip_to_link_mapping[2] = {1};
    chip_to_link_mapping[3] = {2};
  }
  else if (mc == FOUR_CHIP_FOUR_LINK) {
    n_chips = 4; n_elinks = 4;
    chip_to_link_mapping[0] = {0};
    chip_to_link_mapping[1] = {1};
    chip_to_link_mapping[2] = {2};
    chip_to_link_mapping[3] = {3};
  }
  else {
    throw std::invalid_argument("Illegal number of chips and links.");
  }
}


void RD53BDatastream::Trigger(uint64_t timestamp, vector<vector<QCore> > qcores, uint8_t trigger_tag, uint8_t trigger_mask, uint64_t error_mask){
  // Ensure that the input length matches the number of chips
  assert(qcores.size() == n_chips);
  // Output bit streams for each chip
  vector<vector<bool>> chip_bit_streams;
  // Last valid bit in the stream for each chip
  vector<uint8_t> chip_stream_last_bit;
  // To keep track of maximum stream size among all chips
  unsigned long max_stream_length = 0;

  // Generate bit streams for each chip
  for (int ichip=0; ichip < n_chips; ichip++) {
    encoder.reset();
    encoder.serialize_event(qcores[ichip], 0);
    auto stream_chip_raw = encoder.get_stream();
    // Apply AURORA blocking
    auto stream_chip_aurora_unpad = AuroraFormatter::apply_blocking(stream_chip_raw, ichip);
    chip_stream_last_bit.push_back(stream_chip_aurora_unpad.size()%WORDLEN + 1);
    // Apply orphan padding
    auto stream = AuroraFormatter::orphan_pad(stream_chip_aurora_unpad);
    chip_bit_streams.push_back(stream);
    max_stream_length = max(max_stream_length, stream.size());
  }

  // Convert binary streams into vector of uint64_t words
  // Organize the words from per chip to per link
  vector<size_t> word_start_idx(n_chips, 0); // To track the beginning of current 64-bit word
  vector<vector<uint64_t> > link_formated_data(n_elinks, vector<uint64_t>());
  vector<vector<uint8_t> > map_link_data_to_chipID(n_elinks, vector<uint8_t>());
  vector<vector<uint8_t> > link_chip_lastbit(n_elinks, vector<uint8_t>(n_chips, WORDLEN));
  bool all_chips_processed = false;
  while (!all_chips_processed) {
    for (uint8_t ichip=0; ichip < n_chips; ichip++) {
      for (uint8_t ilink : chip_to_link_mapping[ichip]) {
        if (word_start_idx[ichip] + WORDLEN >= chip_bit_streams[ichip].size() + 1) {
          continue;
        }
        auto word_start = chip_bit_streams[ichip].begin()+word_start_idx[ichip];
        uint64_t value = std::accumulate(word_start, word_start + WORDLEN, 0ULL, append_bool_to_uint64);
        link_formated_data[ilink].push_back(value);
        map_link_data_to_chipID[ilink].push_back(ichip);
        link_chip_lastbit[ilink][ichip] = chip_stream_last_bit[ichip];
        word_start_idx[ichip] += WORDLEN;
        if (word_start_idx[ichip] >= max_stream_length) {
          all_chips_processed = true;
          break;
        }
      }
    }
  }

  // create RD53BDataContainers
  vector<RD53BDataContainer> e_link_event_container;
  for (int e_link_id = 0; e_link_id < n_elinks; e_link_id++) {
    RD53BDataContainer container = {
				    n_chips,
				    trigger_tag,
				    trigger_mask,
				    error_mask,
				    timestamp, 
				    link_formated_data[e_link_id],
				    map_link_data_to_chipID[e_link_id],
				    chip_stream_last_bit
    };
    e_link_event_container.push_back(container);
  }
  data_queue.push(e_link_event_container);
}

vector<RD53BDataContainer> RD53BDatastream::GetData(){
  assert(!data_queue.empty());
  vector<RD53BDataContainer> ret_data = data_queue.front();
  data_queue.pop();
  return ret_data;
}
