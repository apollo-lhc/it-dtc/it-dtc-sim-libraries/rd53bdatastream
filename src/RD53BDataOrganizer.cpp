#include <RD53BDataOrganizer.hh>
//#include <fstream>
//#include <memory>
//#include <random>
//#include <bitset>
//#include <limits>
//#include <assert.h>
//#include <stdexcept>
//#include <string.h>
//#include <time.h>
//#include <math.h>
//for it data encoder

#include <interface/QCore.h>



using namespace std;

RD53BDataOrganizer::RD53BDataOrganizer(const char* input_file_path):
tree_unpacker(string(input_file_path), string("BRIL_IT_Analysis/Digis")),
max_nevents(tree_unpacker.get_Nevents()),
generator(seed),
distribution(0, tree_unpacker.get_Nevents()-1)
{
}

void RD53BDataOrganizer::Trigger(uint64_t timestamp, uint8_t trigger_tag, uint8_t trigger_mask){
  TTreeReader::EEntryStatus read_status = tree_unpacker.set_event(distribution(generator));
  int event = tree_unpacker.get_event();
  for(int stream_idx = 0; stream_idx < module_indices.size(); stream_idx++){
    uint16_t module_idx = module_indices[stream_idx];
    RD53BDatastream& stream = streams[stream_idx];
    auto pm = tree_unpacker.get_module(module_idx);
    auto qcores = qfactory.from_pixel_module_perchip(pm);
    int nchips = pm.get_nchips();
    vector<vector<QCore> > stream_qcores;
    for (int chip_id = 0; chip_id < stream.get_nchips(); chip_id++) {
      assert(chip_id < nchips);
      stream_qcores.push_back( qcores[chip_id] );
    }
    stream.Trigger(timestamp, stream_qcores, trigger_tag, trigger_mask);
  }
}

void RD53BDataOrganizer::add_stream(RD53BDatastream::module_config mc, uint16_t module_index) {
  module_indices.push_back(module_index);
  streams.emplace(streams.end(), mc);
}

vector<RD53BDataContainer> RD53BDataOrganizer::GetData(int stream_idx) {
  assert(stream_idx < streams.size());
  return streams[stream_idx].GetData();
}

RD53BDatastream* RD53BDataOrganizer::get_stream(size_t istream) {
  return &(streams[istream]);
}

void RD53BDataOrganizer::set_seed(int _seed) {
  seed = _seed;
  generator.seed (seed);
}
