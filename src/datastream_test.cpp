#include "RD53BSimContainer.hh"

using namespace std;

int main(int argc, char ** argv) {
    // Create one RD53BSimInterface instance
    RD53BSimInterface simulator("test.yaml");

    simulator.Trigger(10, 1, 0x1); //timestamp, triggerTag, triggerMask
    cout << "Event triggered at timestamp 10." << endl;
    
    RD53BSimData simdata_tme10_mod0_chip0 = simulator.GetChipData(10, 0, 0);
    cout << "Get next data for module 0 chip 0 at time 10: " << endl;
    simdata_tme10_mod0_chip0.Print();

    RD53BSimData simdata_tme50_mod0_chip0 = simulator.GetChipData(50, 0, 0);
    cout << "Get next data for module 0 chip 0 at time 50: " << endl;
    simdata_tme50_mod0_chip0.Print();

    RD53BSimData simdata_tme50_mod1_chip2 = simulator.GetChipData(50, 0, 0);
    cout << "Get next data for module 1 chip 2 at time 50: " << endl;
    simdata_tme50_mod1_chip2.Print();

    cout << "Finished testing." << endl;
    return 0;
}
