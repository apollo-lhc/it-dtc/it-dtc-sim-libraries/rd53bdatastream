## RD53BDatastream

A library for generating raw data streams from the RD53B chip.
The code is in C++ and has python bindings for use in cocotb.

## Interface
The setup call controls:
  * The number of e-links used
  * The number of RD53Chips in the chain if non just one.
  * How long to wait before making the data available. 
  * If the triggers all fall on stream boundaries or not. 
  * Other things? 

The Trigger call:
  * takes the 4bit trigger mask and the 6-bit trigger tag to use
  * takes a bit mask of the errors to cause in this event

The GetData call:
  * returns a struct that contains:
    * vector of 64bit words containing the data to be sent. 
    * The timestamp when this data will start to be sent. 
  * DEBUG data including:
    1. the specific trigger in the mask used
    2. the trigger mask this came from
    3. the trigger tag
    4. any data error mask bits used for this data

## Chip Setup
  * This class is intended to hold the following structures
    1. One chip sending data out through 1 or more e-links (round-robin)
    2. Multiple chips sending data through the same e-link

## Usage
Please use git structure discussed [here](https://nvie.com/posts/a-successful-git-branching-model/).
