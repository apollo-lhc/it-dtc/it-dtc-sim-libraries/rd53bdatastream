# location of the Python header files
PYTHON_VERSION = 3.6m
PYTHON_INCLUDE = /usr/include/python$(PYTHON_VERSION)

CXX=g++
CXXFLAGS=-Iinclude -Iit_encoding_rates `root-config --cflags` -g -I${PYTHON_INCLUDE}

TEST = datastream_test
DAN_TEST = dan_test

LIBRARY_RD53SIM_SRCS = src/RD53BDatastream.cpp src/RD53BDataOrganizer.cpp src/RD53BSimContainer.cpp
LIBRARY_RD53SIM_OBJS = $(patsubst src/%.cpp,obj/%.o,${LIBRARY_RD53SIM_SRCS})
LIBRARY_RD53SIM_LINT = $(patsubst src/%.cpp,obj/%.ln,${LIBRARY_RD53SIM_SRCS})
LIBRARY_RD53SIM = lib/libRD53Sim.so

LIB_RD53ENCODING_PATH = it_encoding_rates/lib


LDFLAGS=-L$(LIB_RD53ENCODING_PATH) \
	-Llib \
	-L`root-config --libdir` \
	-L/usr/lib/python$(PYTHON_VERSION)/config

LIBS= 	-lRD53Encoding \
	`root-config  --libs` \
	-lboost_python3 -lpython$(PYTHON_VERSION) \
	-lyaml-cpp \

### LINTER checking ###
LINTER=clang-tidy -extra-arg=-Wno-unknown-warning-option
#second line sets warnings to errors except for certain things
LINTER_DEFAULT_ARGS=-checks=readability-* --warnings-as-errors=readability-*,-readability-implicit-bool-conversion,-readability-else-after-return,-readability-function-cognitive-complexity,-readability-convert-member-functions-to-static,-clang-diagnostic-unknown-warning-option,-readability-make-member-function-const

all: $(TEST) $(DAN_TEST)

lint: ${LIBRARY_RD53SIM_LINT} obj/test.yln

${LIBRARY_RD53SIM} : ${LIBRARY_RD53SIM_OBJS}
	@mkdir -p lib
	$(CXX) -shared ${LDFLAGS} -Wl,--export-dynamic \
	-o $@ ${LIBRARY_RD53SIM_OBJS} ${LIBS}

$(TEST) : obj/$(TEST).o ${LIBRARY_RD53SIM}
	$(CXX) -o $@ $(LDFLAGS) $< ${LIBS} -lRD53Sim 

$(DAN_TEST) : obj/$(DAN_TEST).o ${LIBRARY_RD53SIM}
	$(CXX) -o $@ $(LDFLAGS) $< ${LIBS} -lRD53Sim 


.PHONY: init encoding_submake

obj/%.o: src/%.cpp
	@mkdir -p obj
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(LDFLAGS) -fPIC

obj/%.ln: src/%.cpp
	@mkdir -p obj
	${LINTER} $< ${LINTER_DEFAULT_ARGS} -- $(CXXFLAGS) | tee $@

obj/%.yln: %.yaml
	@mkdir -p obj
	yamllint -d "{extends: default,rules: {document-start: false,trailing-spaces: false,line-length: false,empty-lines: false}}" $<  | tee $@

init:
	git submodule update --init --recursive --remote

encoding_submake:
	make -C $(ENCODING_PATH)

clean:
	${MAKE} -C it_encoding_rates clean
	rm -f $(LIBRARY_RD53SIM_OBJS)
	rm -f $(LIBRARY_RD53SIM_LINK)
	rm -f $(LIBRARY_RD53SIM)
	rm -f $(TEST)
	rm -rf obj

