#!/usr/bin/env python 
import libRD53Sim

simulator = libRD53Sim.RD53BSimInterface("test.yaml")

simulator.Trigger(10,5,0x1)

for i in range(0,30):
    simdata = simulator.GetChipData(i,0,0)
    print("%d %016X" % (i,simdata.GetData(0)))
