#ifndef __RD53B_SIM_CONTAINER_HH__
#define __RD53B_SIM_CONTAINER_HH__

#include <string>
#include <cstdint>
#include <ostream>
#include <iomanip>
#include <RD53BDataOrganizer.hh>
#include <yaml-cpp/yaml.h>

// provide hash function for std::pair
struct hash_pair {
    template <class T1, class T2>
    size_t operator()(const pair<T1, T2>& p) const
    {
        auto hash1 = hash<T1>{}(p.first);
        auto hash2 = hash<T2>{}(p.second);
 
        if (hash1 != hash2) {
            return hash1 ^ hash2;             
        }
         
        // If hash1 == hash2, their XOR is zero.
          return hash1;
    }
};

#define MAX_ELINK_COUNT 4
struct RD53BSimData {
  uint64_t data[MAX_ELINK_COUNT];
  bool valid[MAX_ELINK_COUNT];
  uint64_t GetData(size_t i){return data[i];}
  bool GetValid(size_t i){return valid[i];}
  void Print() {
    std::cout << "data on each E-link : " << std::endl;
    for (size_t i = 0; i < MAX_ELINK_COUNT; i++) {
      std::cout << " 0x" << std::uppercase << std::hex << data[i];
    }
    std::cout << std::endl;
    std::cout << "valid on each E-link : " << std::endl;
    for (size_t i = 0; i < MAX_ELINK_COUNT; i++) {
      std::cout << " " << to_string(valid[i]);
    }
    std::cout << std::endl;
  }
};


class RD53BSimInterface{
public:
  //Loads a config file that builds the datastreams (simple text file)
  RD53BSimInterface(std::string configFile); 

  //Same as DataOrganizer
  void Trigger(uint64_t currentTime,uint8_t triggerTag, uint8_t triggerMask);

  //Acts like a fifo interface and returns a RD53BSimData struct with a valid bool for each of the MAX_ELINK_COUNT possible 64bit words.  (those words should then be considered gone)
  //This will only return data if the curret time is past the timestamp)
  RD53BSimData GetChipData(uint64_t currentTime, size_t moduleID, size_t chipID);
  size_t GetChipCount();
  
private:
  //Pointer to RD53BDataOrganizer that does the actual work of reading data from root file
  RD53BDataOrganizer * dataOrg;
  
  //For each stream added to dataOrg, corresponding moduleID will be added to this vector
  std::vector<size_t> vec_moduleID;

  //Maintain a queue for each chip
  //A queue of pair of timestamp and RD53BSimData
  std::unordered_map<std::pair<size_t, size_t>, std::queue<std::pair<uint64_t, RD53BSimData> >, hash_pair> map_IDs_to_queue;
};


#include <boost/python.hpp> 
using namespace boost::python; 
 
BOOST_PYTHON_MODULE(libRD53Sim) 
{	 
  class_<RD53BSimInterface>("RD53BSimInterface",init<std::string >())
	 .def("Trigger",&RD53BSimInterface::Trigger)
	 .def("GetChipData",&RD53BSimInterface::GetChipData)
	 .def("GetChipCount",&RD53BSimInterface::GetChipCount);
  class_<RD53BSimData>("RD53BSimData")
	 .def("GetValid",&RD53BSimData::GetValid)
	 .def("GetData",&RD53BSimData::GetData);
	 
}
#endif
