#ifndef __RD53B_DATA_ORGANIZER_HH__
#define __RD53B_DATA_ORGANIZER_HH__

#include <algorithm>
#include <iostream>
#include <stdint.h>
#include <vector>
#include "RD53BDatastream.hh"
#include <interface/ModuleTTreeUnpacker.h>
#include <interface/QCoreFactory.h>

using namespace std;

class RD53BDataOrganizer{
public:
  RD53BDataOrganizer(const char* input_file_path);
  // select a random event and generate QCores
  // calls stream->Trigger() for all stream in _streams
  void Trigger(uint64_t timestamp, uint8_t trigger_tag, uint8_t trigger_mask); 
  // Associate a RD53BDatastream to this object, mapped to 1-4 chips on the same module
  void add_stream(RD53BDatastream::module_config mc, uint16_t module_index);
  const size_t get_nstreams() {return streams.size();};
  vector<RD53BDataContainer> GetData(int stream_idx);
  void set_seed(int _seed);
  RD53BDatastream* get_stream(size_t istream);
private:
  // seed for random generator
  int seed=1;
  // Keep the tree unpacker that takes the root file and generate QCores
  ModuleTTreeUnpacker tree_unpacker;
  // Random number generator
  default_random_engine generator;
  uniform_int_distribution<uint64_t> distribution;
  // max number of events in the input root file, so that we can generate random event number within the range
  uint64_t max_nevents;
  QCoreFactory qfactory;
  vector<uint16_t> module_indices;
  // Contains vector of associated RD53BDatastream instances
  vector<RD53BDatastream> streams;
};



#endif
