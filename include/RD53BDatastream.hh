#ifndef RD53BDatastream_HH
#define RD53BDatastream_HH

#include <algorithm>
#include <iostream>
#include <iomanip>
//#include <boost/filesystem.hpp>
#include <fstream>
#include <stdint.h>
#include <memory>
#include <random>
#include <bitset>
#include <map>
#include <limits>
#include <assert.h>
#include <stdexcept>
#include <string.h>
#include <vector>
#include <time.h>
#include <math.h>
#include <queue>
//for it data encoder
#include <interface/RD53StreamEncoder.h>
#include <interface/AuroraFormatter.h>
#include <interface/QCore.h>


using namespace std;

// Container for RD53B stream data
// represents a connection bunch: either 1 chip to multiple e-links or multiple chips to 1 e-link
// !NEVER! multiple chips to multiple e-links
struct RD53BDataContainer
{
  uint8_t nchips;
  uint8_t trigger_tag;
  uint8_t trigger_mask;
  uint64_t error_mask;
  uint64_t timestamp;
  vector<uint64_t> data;
  vector<uint8_t> word_chip_mapping; //same size as data, debug info: which chip does each word belong to
  vector<uint8_t> last_valid_bit; //size=nchips, where in the last word is the last valid bit

  uint64_t GetData(size_t iSample);
  size_t   GetDataSize() const;

  void Print(std::ostream & fout = std::cout) const;
  void PrintData(std::ostream & fout = std::cout);
};

// Class to store triggered data for each module
class RD53BDatastream{
public:
  const static int WORDLEN = 64;
  enum module_config {
    TWO_CHIP_TWO_LINK,
    TWO_CHIP_THREE_LINK,
    TWO_CHIP_SIX_LINK,
    FOUR_CHIP_ONE_LINK,
    FOUR_CHIP_TWO_LINK,
    FOUR_CHIP_THREE_LINK,
    FOUR_CHIP_FOUR_LINK
  };

  RD53BDatastream(module_config mc);
  void Trigger(uint64_t timestamp, vector<vector<QCore> > qcores, uint8_t trigger_tag, uint8_t trigger_mask, uint64_t error_mask=~0ull);
  vector<RD53BDataContainer> GetData(); //size=n_elinks
  uint8_t get_nchips() {
    return n_chips;
  }
  uint8_t get_nlinks() {
    return n_elinks;
  }

  static module_config get_config_from_nchips_nlinks(uint8_t nchips, uint8_t nlinks);

private:
  uint8_t n_chips;
  uint8_t n_elinks;
  map<uint8_t, vector<uint8_t> > chip_to_link_mapping; //chip_to_link_mapping[chip] = {link1, link2, ...}
  bool fake_data = false; //whether data are realistic or faked content with only real size
  queue<vector<RD53BDataContainer> > data_queue;
  RD53StreamEncoder encoder;
  AuroraFormatter aurora;
};


////#include <boost/python.hpp> 
////using namespace boost::python; 
////
////BOOST_PYTHON_MODULE(RD53BDataContainer) 
////{ 
////  class_<RD53BDataContainer>("RD53BDataContainer") 
////    .def("Print",       &RD53BDataContainer::Print) 
////    .def("PrintData",   &RD53BDataContainer::PrintData)
////    .def("GetData",     &RD53BDataContainer::GetData)
////    .def("GetDataSize", &RD53BDataContainer::GetDataSize)
////    ; 
////}; 
////
////BOOST_PYTHON_MODULE(RD53BDatastream) 
////{ 
////  class_<RD53BDatastream>("RD53BDatastream") 
////    .def("Setup",   &RD53BDatastream::Setup) 
////    .def("Trigger", &RD53BDatastream::Trigger)
////    .def("GetData", &RD53BDatastream::GetData) 
////    ; 
////}; 


#endif // RD53BDatastream_HH
